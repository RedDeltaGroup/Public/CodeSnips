# I want to submit a:
  - [ ] bug report
  - [ ] feature request
  - [ ] support request

* **What code or file from the repo are you working with?**


* **What is the current behavior?**


* **What behavior did you expect?**


* *For Bugs*  
  **Please provide your steps to reproduce the bug (screen shots and/or code clips are helpful)**


* *For Features*  
  **What is your motivation / use case for the changing?**


* **Please tell us about your environment:**
  - **Versions of the tools you're using:**  
    [PhpStorm 2020.1 | VS Code | VS 2019 CE]

  - **Browser(s):**  
    [all | Chrome XX | Firefox XX | IE XX | Safari XX | Mobile Chrome XX | Android X.X Web Browser | iOS XX Safari | iOS XX UIWebView | iOS XX WKWebView ]

  - **Other information**  
(e.g. detailed explanation, stacktraces, related issues, suggestions how to fix, links for us to have context, eg. stackoverflow, gitter, etc)
